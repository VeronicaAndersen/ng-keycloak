export interface Duck {
    activity: string,
    type: string,
    participants: number,
    price: number,
    link: string,
}