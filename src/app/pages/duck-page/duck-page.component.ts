import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Duck } from 'src/models/duck';

@Component({
  selector: 'app-duck-page',
  templateUrl: './duck-page.component.html',
  styleUrls: ['./duck-page.component.css']
})
export class DuckPageComponent implements OnInit {
  
  ducks = [];
  constructor(private http: HttpClient) { }
  private _error: string = "error";

  duck: Duck = {
    activity: "Activity",
    type: "",
    participants: 0,
    price: 0,
    link: "",

  }
  ngOnInit(): void {


  }

  public findDuck(): Duck {
    this.http.get<Duck>('http://www.boredapi.com/api/activity')
      .subscribe({
        next: (duckData: Duck) => {
          this.duck = duckData;
          
          return this.duck
        }, error: (error: HttpErrorResponse) => {
          this._error = error.message;
          return this.duck
        }
      })
    return this.duck
  }


}
