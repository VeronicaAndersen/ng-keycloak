import { Component, OnInit } from '@angular/core';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  loginWithKeycloak(): void {
    keycloak.login();

  }
  logoutWithKeycloak(): void {
    keycloak.logout();

  }

  showToken(): void {
    console.log(keycloak.tokenParsed);
    
  }

}
