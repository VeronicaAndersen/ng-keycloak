import { Component, OnInit } from '@angular/core';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  loginWithKeycloak(): void {
    keycloak.login();

  }
  logoutWithKeycloak(): void {
    keycloak.logout();

  }

  showToken(): void {
    console.log(keycloak.tokenParsed);
    
  }
}
